var express = require('express');
var app = express();
var path = require('path');
const https = require('https');

app.set('port', (process.env.PORT || 5000));

app.use(express.static(path.join(__dirname, 'public')));

app.get('/', function(request, response) {
  response.sendFile(path.join(__dirname + '/index.html'));
});

app.get('/index.html', function(request, response) {
  response.sendFile(path.join(__dirname + '/index.html'));
});

app.get('/request/', ({query: { lawNumber} }, response) => {
  const lawUrl = `https://law.lis.virginia.gov/LawPortalWebService/json/CodeofVAGetSectionDetails/${lawNumber}`;
  try {
    https.get(lawUrl, (resp) => {
      let data = '';

      resp.on('data', (chunk) => {
        data += chunk;
      });

      resp.on('end', (out) => {
        response.send(data);
      });

    });
  } catch (e){
    response.send(e);
  }

});

app.listen(app.get('port'), function() {
  console.log('Node app is running on port', app.get('port'));
});
